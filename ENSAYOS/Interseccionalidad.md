
## Definiciones

**Identidad(es)**: La reivindicación de una expresión socio cultural diversa y flexible que se ajuste a las necesidades de una nueva izquierda no monolítica ni centralizada.

**La interseccionalidad** es una propuesta surgida del feminismo negro estadounidense, que explica cómo no se puede comprender la diversidad desde un sólo marco explicativo (género, raza, clase, orientación sexual, etc). Quiero exlorar en especial interseccionalidades con el feminismo.

En los años ochenta el movimiento feminista estadounidense conformado por mujeres afroamericanas, llegaron a la conclusión de que los ejes de racismo y género por separado no bastaban para definir su situación de desigualdad. Kimberle Crenshaw es una de las pioneras en
definir y dar forma al término de interseccionalidad1, con la intención de dar cabida a estas desigualdades cruzadas y en general tener herramientas para definir identidades complejas. En la propia UOC hay un estudio sobre una de las metodologías de definición de interseccionalidad2, los llamados Relief Maps. Está pensado para estudiar tres dimensiones de desigualdad: social, geográfica y psicológica/emocional. María Rodó-Zárate (autora del libro "Interseccionalidad: desigualdades, lugares y emociones") es también la autora de esta metodología desarrollada durante su tesis doctoral. Para el desarrollo de la investigación era importante plasmar las tablas en papel.

1-CRENSHAW, Kimberle; Demarginalizing the Intersection of Race and Sex: A Black Feminist Critique of Antidiscrimination Doctrine, Feminist Theory and Antiracist Politics [1989], en línea: "University of Chicago Legal Forum" [consulta: 14 de marzo de 2022] Disponible en: https://chicagounbound.uchicago.edu/cgi/viewcontent.cgi?article=1052&context=uclf

2-RODÓ-ZÁRATE, Maria; Relief Maps [2017], en línea: UOC [consulta: 14 de marzo de 2022] Disponible
en: https://reliefmaps.cat/en/about 

----

## Sobre la obra de Nan Goldin y su relación con la comunidad queer

La fotógrafa conocida como la fotógrafa de la intimidad underground, se movió por diversos círculos estadounidenses fotografiando temáticas como el género y la sexualidad1. Goldin conoció el movimiento dragqueen a los dieciocho años, en los años setenta2, y se siente prendada: "Yo misma me sentía una reina (queen)...Quería mostrarles lo hermosas que eran" dijo en una entrevista.

La elección de esta etapa de la fotografa para mi estudio sobre identidades, de entre toda su obra, es que hablando de su trabajo con las dragqueens habla de "un tercer sexo": "...Hay una sensación de libertad en tener un deseo que no se ha etiquetado"2 explicaba en los años noventa. En "La cuestión de la identidad cultural" de Stuart Hall, el documento que estamos trabajando para la PEC2, se habla de la identidad sociológica como un modo de establecer un puente entre lo "interior" y lo "exterior", entre el mundo personal y el público. La obra de la fotógrafa, y en concreto su trabajo sobre el mundo de las dragqueens me parece un reflejo de esta identidad.

1. Redacción, febrero de 2019, "Fotógrafas olvidadas", 50mmfotogramas, [consultado el 17 de marzo de 2022], En línea disponible en : https://50mmfotografas.com/fotografas-olvidadas-nan-goldin/

2. MANCHESTER, Elizabeth, 2001, "Jimmy Paulette and Tabboo! undressing, NYC", TATE, [consultado el 17 de marzo de 2022], En línea disponible en: https://www.tate.org.uk/art/artworks/goldin-jimmy-paulette-and-tabboo-undressing-nyc-p11513
